<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HotelBooking</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
        rel="stylesheet">
</head>
<body>
	<br>
	<form action="book" method="POST">
	
	<br/>
	<div class="container">
	<b><font size="10">Vista Hotels</font></b>
	<table class="table table-striped">
	<thead align="center">
	<tr align="center">
	<td>
	<p align="center">
	<font face="timesnewroman"> 
	<b><font color="red">${errorMsg}</font></b>
	</p><br>
	<b></b><font size="10">Booking Details</font></b>
	</td>
	</tr>
	</thead>
	<tbody>
	<tr align="center">
	<td><b>Name</b>&nbsp;&nbsp;&nbsp;<input name="name" type="text" required/></td>
	</tr>
	<tr align="center">
	<td><b>Age</b>&nbsp;&nbsp;&nbsp;<input name="age" type="number" required/></td> &nbsp;
<tr align="center">
	<td><b>Gender</b>&nbsp;&nbsp;&nbsp;<select name=gender>
	<option value="M">Male</option>
	<option value="F">Female</option>
	</select>
	</tr>
	<tr align="center">
	<td><b>Check-in Date</b>&nbsp;&nbsp;&nbsp;<input name="checkin" type="text" placeholder="dd/mm/yyyy" 
	pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"/ required></td>
	</tr>
	<tr align="center">
    <td><b>Check-out Date</b>&nbsp;&nbsp;&nbsp;<input name="checkout" type="text" placeholder="dd/mm/yyyy" 
	pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}"/ required></td>
	</tr>
	<tr align="center">
    <td><b>Room Preference</b>&nbsp;&nbsp;&nbsp;<select name=roompref>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	</select></td>
	</tr>
	<tr align="center">
	<td>
	<input type="submit" name="submit" value="Submit" class="btn btn-info"/>
	</td>
	</tr>
	</font>
	</tbody>
	</table>
	</div>
	</form>
	
</body>
</html>