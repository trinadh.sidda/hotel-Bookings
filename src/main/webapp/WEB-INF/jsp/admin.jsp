<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
        rel="stylesheet">
</head>
<body>
	<br>
	<form action="bookings" method="GET">
	
	<br/>
	<font face="timesnewroman"> 
	<div class="container">
	<b><font size="10">Vista Hotels</font></b>
	<table class="table table-striped">
	<thead align="center">
	<tr align="center">
	<td>
	<b></b><font size="10">Admin</font></b>
	</td>
	</tr>
	</thead>
	<tbody>
	<tr align="center">
	<td>
	<input type="submit" name="submit" value="Get Booking Data" class="btn btn-info"/>
	</td>
	</tr>
	
	</tbody>
	</table>
	<p align="center">
	
	<font color="red">${file}</font>
	</p>
	
	</div>
	<div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>List of Bookings</h2></caption>
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Check-in</th>
                <th>Check-out</th>
                <th>Room Pref</th>
            </tr>
            <c:forEach var="customer" items="${errorMsg}">
                <tr>
                    <td><c:out value="${customer.name}" /></td>
                    <td><c:out value="${customer.age}" /></td>
                    <td><c:out value="${customer.gender}" /></td>
                    <td><c:out value="${customer.checkin}" /></td>
                    <td><c:out value="${customer.checkout}" /></td>
                    <td><c:out value="${customer.roompref}" /></td>
                </tr>
            </c:forEach>
        </table>
    </div>
	</form>
	
</body>
</html>