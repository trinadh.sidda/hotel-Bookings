<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
        rel="stylesheet">
</head>
<body>
	<br>
	<form action="submit" method="POST">
	<p align="center">
	<font face="timesnewroman"> 
	<font color="red">${errorMsg}</font>
	</p>
	<br/>
	<div class="container">
	
	<table class="table table-striped">
	<thead align="center">
	<tr align="center">
	<td>
	<b></b><font size="10">Login</font></b>
	</td>
	</tr>
	</thead>
	<tbody>
	<tr align="center">
	<td><b>Username</b>&nbsp;&nbsp;&nbsp;<input name="userName" type="text"/></td>
	</tr>
	<tr align="center">
	<td><b>Password</b>&nbsp;&nbsp;&nbsp;<input name="password" type="password"/></td>
	
	</tr>
	<tr align="center">
	<td>
	<input type="submit" name="submit" value="Signin" class="btn btn-info"/>
	<b><a href="/signup" class="btn btn-info">Signup</a></b>
	</td>
	</tr>
	</font>
	</tbody>
	</table>
	</div>
	</form>
	<%-- <form action="signup">
	<br/>
	<input type="/submit" value="Signup"/>
	</form> --%>
	
</body>
</html>