package com.vista.service;

import java.util.List;

import com.vista.model.User;



public interface RegisterService {

	public void saveRegister(User user);
	public List<User> getAllUsers();
}
