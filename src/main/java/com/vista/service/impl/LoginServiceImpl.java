package com.vista.service.impl;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.vista.model.User;
import com.vista.repo.UserRepo;
import com.vista.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private UserRepo userRepo;
	
	@Value("${admin.name:admin}")
	private String adminName;
	@Value("${admin.password:admin}")
	private String adminPassword;
	
	@PostConstruct
	public void init(){
		User user = new User();
		user.setUserName(adminName);
		user.setPassword(adminPassword.toCharArray());
		userRepo.save(user);
	}
	public int validateUser(User user) {
		
		if(!userRepo.findByUserName(user.getUserName()).isPresent()){
			//userRepo.save(user);
			return 0;
		}
			
		
		User userData = userRepo.findByUserName(user.getUserName()).get();
		
		return 1;


		
	}

	public UserRepo getUserRepo() {
		return userRepo;
	}

	public void setUserRepo(UserRepo userRepo) {
		this.userRepo = userRepo;
	}


}
