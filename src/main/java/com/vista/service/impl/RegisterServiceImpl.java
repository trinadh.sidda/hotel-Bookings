package com.vista.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vista.model.User;
import com.vista.repo.UserRepo;
import com.vista.service.RegisterService;

@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	private UserRepo userRepo;
	
	public void saveRegister(User customer) {

		userRepo.save(customer);
	}

	public List<User> getAllUsers() {
		return null;
	}
	

}
