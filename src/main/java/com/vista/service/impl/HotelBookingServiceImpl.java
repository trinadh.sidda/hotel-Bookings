package com.vista.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vista.model.Customer;
import com.vista.repo.CustomerRepo;
import com.vista.service.HotelBookingService;

@Service
public class HotelBookingServiceImpl implements HotelBookingService {
	
	@Autowired
	private CustomerRepo customerRepo;
	

	@Override
	public void save(Customer customer) {
		customerRepo.save(customer);
		
	}


	@Override
	public List<Customer> getBookings() {
		return customerRepo.findAll();
		
	}

}
