package com.vista.service;

import java.util.List;
import java.util.Map;

import com.vista.model.User;

public interface AccountService {

	User getUserDetails(User user) throws Exception;

	User getUserById(int userId) throws Exception;
	
	
}
