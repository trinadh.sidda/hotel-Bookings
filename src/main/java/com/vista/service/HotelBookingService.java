package com.vista.service;

import java.util.List;

import com.vista.model.Customer;

public interface HotelBookingService {

	void save(Customer customer);

	List<Customer> getBookings();

}
