package com.vista.controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.vista.model.Customer;
import com.vista.service.HotelBookingService;

@Controller
public class HotelBookingController {

	@Autowired
	private HotelBookingService hotelBooking;
	
	@Value("${booking.file.loc}")
	private String fileLoc;
	
	@RequestMapping(value = "/book", method = RequestMethod.POST)
	public String bookingDetails(@ModelAttribute("customer") Customer customer, HttpServletRequest request, Model model) {
		
		hotelBooking.save(customer);
		
		model.addAttribute("errorMsg", "Booking Details Submited");
		return "hotel";
	}
	
	@RequestMapping(value = "/bookings", method = RequestMethod.GET)
	public String fetchBookings(HttpServletRequest request, Model model){
		
		model.addAttribute("errorMsg", hotelBooking.getBookings());
		
		String filename = "Bookingdata_"
				+ LocalDateTime.now().format(
						DateTimeFormatter.ofPattern("ddMMyyyy_HHmMss"))
				+ ".csv";
		
		if(!ObjectUtils.isEmpty(hotelBooking.getBookings())){
			model.addAttribute("file", String.format("Booking Data file created: %s", filename) );
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(fileLoc + filename,true));
				bw.append("Name,Age,Gender,Check-in,Check-out,RoomPref\n");
				for(Customer customer : hotelBooking.getBookings()){
					bw.append(customer + "\n");
				}
				bw.close();
			} catch (Exception e) {
				model.addAttribute("file", "Failed to create Booking data file !!!");
			}

		}
		else{
			model.addAttribute("file", "No Bookings made");
		}
		return "admin";
	}

}
