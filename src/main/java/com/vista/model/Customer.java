package com.vista.model;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Customer {
	
	@Id
	private String name;
	private int age;
	private String gender;
	private String checkin;
	private String checkout;
	private String roompref;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public String getRoompref() {
		return roompref;
	}
	public void setRoompref(String roompref) {
		this.roompref = roompref;
	}
	@Override
	public String toString() {
		return name + "," + age + "," + gender
				+ "," + checkin + "," + checkout
				+ "," + roompref;
	}
	

}
