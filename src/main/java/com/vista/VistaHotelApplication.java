package com.vista;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VistaHotelApplication {

	public static void main(String[] args) {
		SpringApplication.run(VistaHotelApplication.class, args);
	}

}
