package com.vista.dao;

import com.vista.model.User;

public interface LoginDao {
	public int validateUser(User user);
}
