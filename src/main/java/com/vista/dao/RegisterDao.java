package com.vista.dao;

import java.util.List;

import com.vista.model.User;

public interface RegisterDao {
	public void saveRegister(User user);

	public List<User> getAllUsers();
}
