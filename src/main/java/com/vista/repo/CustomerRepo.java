package com.vista.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vista.model.Customer;

public interface CustomerRepo extends JpaRepository<Customer, String> {

}
